import globalSettings
import temporarySettings
import gameState
import gameTree
import random

def generateNewSettings():
	random.seed()
	temporarySettings.cornerReward = globalSettings.cornerReward * random.uniform(0.75, 1.25)
	temporarySettings.edgeReward = globalSettings.edgeReward * random.uniform(0.75, 1.25)
	temporarySettings.punishment = globalSettings.punishment * random.uniform(0.75, 1.25)
	temporarySettings.closeToCornerPunishment = globalSettings.closeToCornerPunishment * random.uniform(0.75, 1.25)
	temporarySettings.multipleCornerReward = globalSettings.multipleCornerReward * random.uniform(0.75, 1.25)
	temporarySettings.cornerChainReward = globalSettings.cornerChainReward * random.uniform(0.75, 1.25)
	temporarySettings.cornerChainExponent = globalSettings.cornerChainExponent * random.uniform(0.75, 1.25)
	temporarySettings.constMultiplier = globalSettings.constMultiplier * random.uniform(0.75, 1.25)
	temporarySettings.constExponent = globalSettings. constExponent * random.uniform(0.75, 1.25)

def printBestSettings():
	print("treeDepth = " + str(temporarySettings.treeDepth))
	print("numberOfProcesses = " + str(temporarySettings.numberOfProcesses))
	print("cornerReward = " + str(temporarySettings.cornerReward))
	print("edgeReward = " + str(temporarySettings.edgeReward))
	print("punishment = " + str(temporarySettings.punishment))
	print("closeToCornerPunishment = " + str(temporarySettings.closeToCornerPunishment))
	print("multipleCornerReward = " + str(temporarySettings.multipleCornerReward))
	print("cornerChainReward = " + str(temporarySettings.cornerChainReward))
	print("cornerChainExponent = " + str(temporarySettings.cornerChainExponent))
	print("constMultiplier = " + str(temporarySettings.constMultiplier))
	print("constExponent = " + str(temporarySettings.constExponent))

def updateSettings():
	globalSettings.cornerReward = temporarySettings.cornerReward
	globalSettings.edgeReward = temporarySettings.edgeReward
	globalSettings.punishment = temporarySettings.punishment
	globalSettings.closeToCornerPunishment = temporarySettings.closeToCornerPunishment
	globalSettings.multipleCornerReward = temporarySettings.multipleCornerReward
	globalSettings.cornerChainReward = temporarySettings.cornerChainReward
	globalSettings.cornerChainExponent = temporarySettings.cornerChainExponent
	globalSettings.constMultiplier = temporarySettings.constMultiplier
	globalSettings.constExponent = temporarySettings. constExponent

def playGame(player):
	newgame = gameState.GameState()
	while(len(newgame.getAllPossibilities(1)) > 0 or len(newgame.getAllPossibilities(2)) > 0):
		if(newgame.getAllPossibilities(1)):
			newgame.putStone(*gameTree.GameTree(newgame, 1, player == 1).getRecommendedMove(player == 1), 1)
		if(newgame.getAllPossibilities(2)):
			newgame.putStone(*gameTree.GameTree(newgame, 2, player == 2).getRecommendedMove(player == 2), 2)
	if(newgame.getPoints(1) > newgame.getPoints(2)):
		return 1
	return 2

def playMultipleGames(numberOfGames):
	assert(isinstance(numberOfGames, int))
	assert(numberOfGames > 0)
	games = numberOfGames
	while(numberOfGames > 0):
		generateNewSettings()
		if(playGame(1) == 1 and playGame(2) == 2):
			print("New settings, game number " + str(games - numberOfGames + 1))
			updateSettings()
		numberOfGames -= 1
	printBestSettings()
