import globalSettings
import temporarySettings

class GameState:
	def __init__(self):
		self.board = []
		for i in range(8):
			self.board.append([0,0,0,0,0,0,0,0])
		self.board[3][3] = 2
		self.board[4][4] = 2
		self.board[3][4] = 1
		self.board[4][3] = 1
		self.turnsLeft = 60

	def getEnemyNumber(self, player):
		assert(player in range(1, 3))
		return 2 if player == 1 else 1

	def printState(self):
		for i in range(8):
			print(self.board[i])

	def getAllPossibilities(self, player):
		ret = []
		assert(player == 1 or player == 2)
		for x in range(8):
			for y in range(8):
				if(self.canTakeField(x,y, player)):
					ret.append((x, y))
		return ret

	def canTakeField(self, x, y, player):
		assert(player == 1 or player == 2)
		enemy = 2 if player == 1 else 1
		if(self.board[x][y] == 0):
			prev = player
			for i in reversed(range(x)):
				if(self.board[i][y] == 0):
					break
				if(self.board[i][y] == enemy):
					prev = enemy
				if(self.board[i][y] == player):
					if(prev == enemy):
						return True
					else:
						break
			prev = player
			for i in range(x+1, 8):
				if(self.board[i][y] == 0):
					break
				if(self.board[i][y] == enemy):
					prev = enemy
				if(self.board[i][y] == player):
					if(prev == enemy):
						return True
					else:
						break
			prev = player
			for i in reversed(range(y)):
				if(self.board[x][i] == 0):
					break
				if(self.board[x][i] == enemy):
					prev = enemy
				if(self.board[x][i] == player):
					if(prev == enemy):
						return True
					else:
						break
			prev = player
			for i in range(y+1, 8):
				if(self.board[x][i] == 0):
					break
				if(self.board[x][i] == enemy):
					prev = enemy
				if(self.board[x][i] == player):
					if(prev == enemy):
						return True
					else:
						break

			tmpX = x + 1
			tmpY = y + 1
			prev = player
			while(tmpX < 8 and tmpY < 8):
				if(self.board[tmpX][tmpY] == 0):
					break
				if(self.board[tmpX][tmpY] == enemy):
					prev = enemy
				if(self.board[tmpX][tmpY] == player):
					if(prev == enemy):
						return True
					else:
						break
				tmpX = tmpX + 1
				tmpY = tmpY + 1

			tmpX = x + 1
			tmpY = y - 1
			prev = player
			while(tmpX < 8 and tmpY >= 0):
				if(self.board[tmpX][tmpY] == 0):
					break
				if(self.board[tmpX][tmpY] == enemy):
					prev = enemy
				if(self.board[tmpX][tmpY] == player):
					if(prev == enemy):
						return True
					else:
						break
				tmpX = tmpX + 1
				tmpY = tmpY - 1

			tmpX = x - 1
			tmpY = y - 1
			prev = player
			while(tmpX >= 0 and tmpY >= 0):
				if(self.board[tmpX][tmpY] == 0):
					break
				if(self.board[tmpX][tmpY] == enemy):
					prev = enemy
				if(self.board[tmpX][tmpY] == player):
					if(prev == enemy):
						return True
					else:
						break
				tmpX = tmpX - 1
				tmpY = tmpY - 1

			tmpX = x - 1
			tmpY = y + 1
			prev = player
			while(tmpX >= 0 and tmpY < 8):
				if(self.board[tmpX][tmpY] == 0):
					break
				if(self.board[tmpX][tmpY] == enemy):
					prev = enemy
				if(self.board[tmpX][tmpY] == player):
					if(prev == enemy):
						return True
					else:
						break
				tmpX = tmpX - 1
				tmpY = tmpY + 1


		return False

	@staticmethod
	def duplicateState(currentState):
		newState = GameState()
		for i in range(8):
			newState.board[i] = list(currentState.board[i])
			# for ii in range(8):
			# 	newState.board[i][ii]=currentState.board[i][ii]
		return newState

	def putStone(self, x, y, player):
		assert(x in range(8) and y in range(8) and player in range(1,3))
		assert((x,y) in self.getAllPossibilities(player))
		self.board[x][y] = player
		self.turnsLeft = self.turnsLeft - 1
		fieldsToChange = []
		shouldClearList = True
		for i in reversed(range(x)):
			if(self.board[i][y] == self.getEnemyNumber(player)):
				fieldsToChange.append((i, y))
			else:
				if(self.board[i][y] == player):
					shouldClearList = False
				break
		if(shouldClearList):
			fieldsToChange = []
		for field in fieldsToChange:
			self.board[field[0]][field[1]] = player

		fieldsToChange = []
		shouldClearList = True
		for i in range(x+1, 8):
			if(self.board[i][y] == self.getEnemyNumber(player)):
				fieldsToChange.append((i, y))
			else:
				if(self.board[i][y] == player):
					shouldClearList = False
				break
		if(shouldClearList):
			fieldsToChange = []
		for field in fieldsToChange:
			self.board[field[0]][field[1]] = player

		fieldsToChange = []
		shouldClearList = True
		for i in reversed(range(y)):
			if(self.board[x][i] == self.getEnemyNumber(player)):
				fieldsToChange.append((x, i))
			else:
				if(self.board[x][i] == player):
					shouldClearList = False
				break
		if(shouldClearList):
			fieldsToChange = []
		for field in fieldsToChange:
			self.board[field[0]][field[1]] = player

		fieldsToChange = []
		shouldClearList = True
		for i in range(y+1, 8):
			if(self.board[x][i] == self.getEnemyNumber(player)):
				fieldsToChange.append((x, i))
			else:
				if(self.board[x][i] == player):
					shouldClearList = False
				break
		if(shouldClearList):
			fieldsToChange = []
		for field in fieldsToChange:
			self.board[field[0]][field[1]] = player

		newX = x + 1
		newY = y + 1
		fieldsToChange = []
		shouldClearList = True
		while(newX < 8 and newY < 8):
			if(self.board[newX][newY] == self.getEnemyNumber(player)):
				fieldsToChange.append((newX, newY))
			else:
				if(self.board[newX][newY] == player):
					shouldClearList = False
				break
			newX = newX + 1
			newY = newY + 1
		if(shouldClearList):
			fieldsToChange = []
		for field in fieldsToChange:
			self.board[field[0]][field[1]] = player

		newX = x - 1
		newY = y + 1
		fieldsToChange = []
		shouldClearList = True
		while(newX >= 0 and newY < 8):
			if(self.board[newX][newY] == self.getEnemyNumber(player)):
				fieldsToChange.append((newX, newY))
			else:
				if(self.board[newX][newY] == player):
					shouldClearList = False
				break
			newX = newX - 1
			newY = newY + 1
		if(shouldClearList):
			fieldsToChange = []
		for field in fieldsToChange:
			self.board[field[0]][field[1]] = player

		newX = x + 1
		newY = y - 1
		fieldsToChange = []
		shouldClearList = True
		while(newX < 8 and newY >= 0):
			if(self.board[newX][newY] == self.getEnemyNumber(player)):
				fieldsToChange.append((newX, newY))
			else:
				if(self.board[newX][newY] == player):
					shouldClearList = False
				break
			newX = newX + 1
			newY = newY - 1
		if(shouldClearList):
			fieldsToChange = []
		for field in fieldsToChange:
			self.board[field[0]][field[1]] = player

		newX = x - 1
		newY = y - 1
		fieldsToChange = []
		shouldClearList = True
		while(newX >= 0 and newY >= 0):
			if(self.board[newX][newY] == self.getEnemyNumber(player)):
				fieldsToChange.append((newX, newY))
			else:
				if(self.board[newX][newY] == player):
					shouldClearList = False
				break
			newX = newX - 1
			newY = newY - 1
		if(shouldClearList):
			fieldsToChange = []
		for field in fieldsToChange:
			self.board[field[0]][field[1]] = player

	def getPoints(self, player):
		assert(player in range(1, 3))
		points = 0
		for x in range(8):
			for y in range(8):
				if(self.board[x][y] == player):
					points = points + 1
				elif(self.board[x][y] != 0):
					points = points - 1
		return points

	def getDiscountedPoints(self, player, temporary = False):
		module = temporarySettings if temporary else globalSettings
		assert(player in range(1, 3))
		enemy = 1 if player == 2 else 2
		multiplier = (module.constMultiplier * self.getTurnsLeft() / 60.0) ** module.constExponent
		points = 0
		for x in range(8):
			for y in range(8):
				if(self.board[x][y] == player):
					points = points + self.getDiscountedValue(x, y, player, multiplier, temporary)
				elif(self.board[x][y] != 0):
					points = points - self.getDiscountedValue(x, y, enemy, multiplier, temporary)
		points = points + (multiplier * (module.multipleCornerReward ** self.getNumberOfCorners(player)))
		return points

	def getTurnsLeft(self):
		return self.turnsLeft

	def getNumberOfCorners(self, player):
		number = 0
		if(self.board[0][0] == player):
			number = number + 1
		if(self.board[7][0] == player):
			number = number + 1
		if(self.board[0][7] == player):
			number = number + 1
		if(self.board[7][7] == player):
			number = number + 1
		return number

	def getDiscountedValue(self, x, y, player, baseFactor, temporary = False):
		module = temporarySettings if temporary else globalSettings
		enemy = 1 if player == 2 else 2
		# baseFactor = (module.constMultiplier * self.getTurnsLeft() / 60.0) ** module.constExponent
		result = 1.0
		if(enemy in self.surroundingsInVerticalLine(x, y, player) and 0 in self.surroundingsInVerticalLine(x, y, player)):
			result = result - baseFactor * module.punishment
		if(enemy in self.surroundingsInHorizontalLine(x, y, player) and 0 in self.surroundingsInHorizontalLine(x, y, player)):
			result = result - baseFactor * module.punishment
		if(enemy in self.surroundingsInCross1Line(x, y, player) and 0 in self.surroundingsInCross1Line(x, y, player)):
			result = result - baseFactor * module.punishment
		if(enemy in self.surroundingsInCross2Line(x, y, player) and 0 in self.surroundingsInCross2Line(x, y, player)):
			result = result - baseFactor * module.punishment
		if(self.isCorner(x, y)):
			result = result + module.cornerReward * baseFactor
		elif(self.isEdge(x,y)):
			result = result + module.edgeReward * baseFactor
		if(self.isCloseToEmptyCorner(x, y)):
			result = result - (module.closeToCornerPunishment + module.edgeReward) * baseFactor
		return result

	def isCloseToEmptyCorner(self, x, y):
		points = []
		if (self.board[0][0] == 0):
			points.append((1, 1))
			points.append((0, 1))
			points.append((1, 0))
		if (self.board[7][7] == 0):
			points.append((7, 6))
			points.append((6, 7))
			points.append((6, 6))
		if (self.board[0][7] == 0):
			points.append((1, 6))
			points.append((0, 6))
			points.append((1, 7))
		if (self.board[7][0] == 0):
			points.append((6, 1))
			points.append((7, 1))
			points.append((6, 0))

		# points = [(1, 1), (0, 1), (1, 0), (7, 6), (6, 7), (6, 6), (1, 6), (1, 7), (0, 6), (6, 1), (6, 0), (7, 1)]
		if((x, y) in points):
			return True
		return False

	def isCorner(self, x, y):
		point = (x, y)
		if(point == (0, 0) or point == (7, 0) or point == (0, 7) or point == (7, 7)):
			return True
		return False

	def isEdge(self, x, y):
		if(x == 0 or x == 7 or y == 7 or y == 0):
			return True
		return False

	def surroundingsInVerticalLine(self, x, y, player):
		enemy = 1 if player == 2 else 2
		newX = x + 1
		result = []
		while(newX < 8):
			if(self.board[newX][y] != player):
				result.append(self.board[newX][y])
				break
			newX = newX + 1
		newX = x - 1
		while(newX >= 0):
			if(self.board[newX][y] != player):
				result.append(self.board[newX][y])
				break
			newX = newX - 1
		return result

	def surroundingsInHorizontalLine(self, x, y, player):
		enemy = 1 if player == 2 else 2
		newY = y + 1
		result = []
		while(newY < 8):
			if(self.board[x][newY] != player):
				result.append(self.board[x][newY])
				break
			newY = newY + 1
		newY = y - 1
		while(newY >= 0):
			if(self.board[x][newY] != player):
				result.append(self.board[x][newY])
				break
			newY = newY - 1
		return result

	def surroundingsInCross1Line(self, x, y, player):
		enemy = 1 if player == 2 else 2
		newX = x + 1
		newY = y + 1
		result = []
		while(newX < 8 and newY < 8):
			if(self.board[newX][newY] != player):
				result.append(self.board[newX][newY])
				break
			newY = newY + 1
			newX = newX + 1
		newY = y - 1
		newX = x - 1
		while(newY >= 0 and newX >= 0):
			if(self.board[newX][newY] != player):
				result.append(self.board[newX][newY])
				break
			newY = newY - 1
			newX = newX - 1
		return result

	def surroundingsInCross2Line(self, x, y, player):
		enemy = 1 if player == 2 else 2
		newX = x + 1
		newY = y - 1
		result = []
		while(newX < 8 and newY >= 0):
			if(self.board[newX][newY] != player):
				result.append(self.board[newX][newY])
				break
			newY = newY - 1
			newX = newX + 1
		newY = y + 1
		newX = x - 1
		while(newY < 8 and newX >= 0):
			if(self.board[newX][newY] != player):
				result.append(self.board[newX][newY])
				break
			newY = newY + 1
			newX = newX - 1
		return result

	def getCornerChainLenghts(self, player):
		result = []
		#corner (0, 0)
		x = 0
		y = 0
		while(x < 8):
			if(self.board[x][y] != player):
				break
			x += 1
		result.append(x)

		x = 0
		y = 0
		while(y < 8):
			if(self.board[x][y] != player):
				break
			y += 1
		result.append(y)

		#corner (0, 7)
		x = 0
		y = 7
		while(x < 8):
			if(self.board[x][y] != player):
				break
			x += 1
		result.append(x)

		x = 0
		y = 7
		while(y >= 0):
			if(self.board[x][y] != player):
				break
			y -= 1
		result.append(7 - y)

		#corner (7, 0)
		x = 7
		y = 0
		while(x >= 0):
			if(self.board[x][y] != player):
				break
			x -= 1
		result.append(7 - x)

		x = 7
		y = 0
		while(y < 8):
			if(self.board[x][y] != player):
				break
			y += 1
		result.append(y)

		#corner (7, 7)
		x = 7
		y = 7
		while(x >= 0):
			if(self.board[x][y] != player):
				break
			x -= 1
		result.append(7 - x)

		x = 7
		y = 7
		while(y >= 0):
			if(self.board[x][y] != player):
				break
			y -= 1
		result.append(7 - y)
		return result
