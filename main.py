#!/usr/bin/env python
import gameState
import gameTree
import sys
import selfLearningEngine
import cProfile

def playNormalGame():
	newgame = gameState.GameState()

	while(len(newgame.getAllPossibilities(1)) > 0 or len(newgame.getAllPossibilities(2)) > 0):
		print(gameTree.GameTree(newgame, 1, False).getRecommendedMove(False))
		if(newgame.getAllPossibilities(1)):
			x,y = map(int,sys.stdin.readline().split())
			newgame.putStone(x,y,1)
		if(newgame.getAllPossibilities(2)):
			x,y = map(int,sys.stdin.readline().split())
			newgame.putStone(x,y,2)
		if(not newgame.getAllPossibilities(1) and not newgame.getAllPossibilities(2)):
			break
		newgame.printState()

if __name__ == "__main__":
	selfLearningEngine.playMultipleGames(int(sys.argv[1]))
	# selfLearningEngine.playGame()
	# cProfile.run('selfLearningEngine.playMultipleGames(10)')
