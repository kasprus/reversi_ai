#!/bin/bash
#This script is using cython to generate .so files of python modules to improve performace of the program
rm -R -f -v ccode
mkdir -p -v ccode

for f in *.py
do
	if [ "$f" != "setup.py" ] && [ "$f" != "main.py" ]
	then
		cp -v $f "ccode/$f""x"
	fi
done

python setup.py build_ext --inplace
