from distutils.core import setup
from Cython.Build import cythonize

setup(
    name = "ReversiTest",
    ext_modules = cythonize("ccode/*.pyx"),
)
