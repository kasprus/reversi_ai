import globalSettings
import temporarySettings
from gameState import GameState
import sys
from concurrent.futures import ProcessPoolExecutor

class GameNode:
	def __init__(self, currentState, level, player = 0):
		self.state = currentState
		self.children = []
		self.level = level
		self.move = (-1, -1)
		self.player = player
	def addMove(self, move):
		self.move = move
	def getMove(self):
		return self.move
	def getLevel(self):
		return self.level


class GameTree:
	def __init__(self, currentState, player, temporary):
		self.head = GameNode(currentState, 1)
		self.player = player
		module = temporarySettings if temporary else globalSettings
		if(len(self.head.state.getAllPossibilities(player)) == 0):
			player = 1 if self.player == 2 else 2
		for move in self.head.state.getAllPossibilities(player):
			newState = GameState.duplicateState(self.head.state)
			# print(move)
			newState.putStone(*move, player)
			newNode = GameNode(newState, 2, player)
			newNode.addMove(move)
			self.head.children.append(newNode)

	def getRecommendedMove(self, temporary = False):
		module = temporarySettings if temporary else globalSettings
		enemy = 1 if self.player == 2 else 2
		move = (-1, -1)
		result = -1000000.0
		evalList = []
		if(module.numberOfProcesses != 0):
			with ProcessPoolExecutor(module.numberOfProcesses) as executor:
				llen = len(self.head.children)
				evalList = list(executor.map(evalFunc, self.head.children, [self.player] * llen, [temporary] * llen))
		else:
			llen = len(self.head.children)
			evalList =  list(map(evalFunc, self.head.children, [self.player] * llen, [temporary] * llen))
		for res in evalList:
			if(res[1] > result):
				result = res[1]
				move = res[0]
		# print(result)
		return move



def generateNodes(node, level, player, temporary):
	ret = []
	module = temporarySettings if temporary else globalSettings
	if(level < module.treeDepth):
		if(not node.state.getAllPossibilities(player)):
			player = 1 if player == 2 else 2
		enemy = 1 if player == 2 else 2

		for move in node.state.getAllPossibilities(player):
			newState = GameState.duplicateState(node.state)
			newState.putStone(*move, player)
			newNode = GameNode(newState, level + 1, player)
			newNode.addMove(move)
			ret.append(newNode)
	return ret

def evaluate(node, level, player, temporary = False):
	module = temporarySettings if temporary else globalSettings
	enemy = 1 if node.player == 2 else 2
	if(node.level == module.treeDepth or (len(node.state.getAllPossibilities(1)) == 0 and len(node.state.getAllPossibilities(2)) == 0)):
		return node.state.getDiscountedPoints(player, temporary)
	node.children = generateNodes(node, level, enemy if node.state.getAllPossibilities(enemy) else node.player, temporary)
	llen = len(node.children)
	if(node.player == player and len(node.state.getAllPossibilities(enemy)) > 0):
		return min(map(evaluate, node.children, [level + 1] * llen, [player] * llen, [temporary] * llen))
		# return min([evaluate(child, level + 1, player, temporary) for child in node.children])
	# return max([evaluate(child, level + 1, player, temporary) for child in node.children])
	return max(map(evaluate, node.children, [level + 1] * llen, [player] * llen, [temporary] * llen))

def evalFunc(child, player, temporary = False):

	return (child.move, evaluate(child, 2, player, temporary))
